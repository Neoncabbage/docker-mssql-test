IF NOT EXISTS (SELECT TOP 1 * FROM sys.databases WHERE name = 'SALMessaging_Cloud') BEGIN
	CREATE DATABASE SALMessaging_Cloud;
END

USE SALMessaging_Cloud
GO

CREATE SCHEMA [SCH]
GO

CREATE SCHEMA [WKW]
GO

CREATE ROLE [ScheduleRole]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SCH].[ExternalSystem](
	[ScheduleExternalSystemID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[EntityID] [int] NULL,
 CONSTRAINT [PK_ExternalSystem] PRIMARY KEY CLUSTERED 
(
	[ScheduleExternalSystemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [SCH].[Frequency]    Script Date: 18/02/2023 12:05:55 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SCH].[Frequency](
	[FrequencyID] [int] IDENTITY(1,1) NOT NULL,
	[Created] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedBy] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[SystemName] [nvarchar](50) NOT NULL,
	[DelayBeforeOrangeIndicator] [int] NULL,
	[DelayBeforeRedIndicator] [int] NULL,
 CONSTRAINT [PK_tblFrequency] PRIMARY KEY CLUSTERED 
(
	[FrequencyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [SCH].[Instance]    Script Date: 18/02/2023 12:05:55 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SCH].[Instance](
	[InstanceID] [int] IDENTITY(1,1) NOT NULL,
	[Created] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModfiiedBy] [nvarchar](50) NOT NULL,
	[Server] [nvarchar](50) NOT NULL,
	[Domain] [nvarchar](50) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Default] [bit] NULL,
	[ServiceName] [nvarchar](50) NOT NULL,
	[Enabled] [bit] NULL,
 CONSTRAINT [PK_tblInstance] PRIMARY KEY CLUSTERED 
(
	[InstanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [SCH].[PerformanceLog]    Script Date: 18/02/2023 12:05:55 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SCH].[PerformanceLog](
	[PerformanceLogID] [int] IDENTITY(1,1) NOT NULL,
	[Created] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedBy] [nvarchar](50) NOT NULL,
	[Cpu] [int] NOT NULL,
	[Memory] [int] NOT NULL,
	[InstanceID] [int] NULL,
 CONSTRAINT [PK_PerformanceLog] PRIMARY KEY CLUSTERED 
(
	[PerformanceLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [SCH].[Schedule]    Script Date: 18/02/2023 12:05:55 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SCH].[Schedule](
	[ScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[Created] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedBy] [nvarchar](50) NOT NULL,
	[Name] [varchar](500) NULL,
	[Description] [nvarchar](2000) NULL,
	[FrequencyID] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[ScheduleFrequency] [smallint] NOT NULL,
	[EveryNumberOfWeeks] [smallint] NULL,
	[Monday] [bit] NULL,
	[Tuesday] [bit] NULL,
	[Wednesday] [bit] NULL,
	[Thursday] [bit] NULL,
	[Friday] [bit] NULL,
	[Saturday] [bit] NULL,
	[Sunday] [bit] NULL,
	[LastRunCompleted] [datetime] NULL,
	[ScheduleGroupID] [int] NULL,
	[NextRunTime] [datetime] NULL,
	[InstanceID] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[EntityID] [int] NULL,
	[RunBetweenStartTime] [datetime] NULL,
	[RunBetweenEndTime] [datetime] NULL,
	[RestartOnThreshHold] [bit] NULL,
	[DelayBetweenSteps] [int] NULL,
	[EnableDebug] [bit] NOT NULL,
 CONSTRAINT [PK_tblSchedule] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [SCH].[ScheduleGroup]    Script Date: 18/02/2023 12:05:55 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SCH].[ScheduleGroup](
	[ScheduleGroupID] [int] IDENTITY(1,1) NOT NULL,
	[Created] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedBy] [nvarchar](50) NOT NULL,
	[Name] [varchar](500) NULL,
	[EntityID] [int] NULL,
	[Status] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ScheduleGroup] PRIMARY KEY CLUSTERED 
(
	[ScheduleGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [SCH].[ScheduleGroupExternalSystem]    Script Date: 18/02/2023 12:05:55 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SCH].[ScheduleGroupExternalSystem](
	[ScheduleGroupExternalSystemID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleGroupID] [int] NOT NULL,
	[ScheduleExternalSystemID] [int] NOT NULL,
	[KeyValue] [varchar](500) NOT NULL,
 CONSTRAINT [PK_ScheduleGroupExternalSystem] PRIMARY KEY CLUSTERED 
(
	[ScheduleGroupID] ASC,
	[ScheduleExternalSystemID] ASC,
	[KeyValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [SCH].[ScheduleStep]    Script Date: 18/02/2023 12:05:55 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SCH].[ScheduleStep](
	[ScheduleStepID] [int] IDENTITY(1,1) NOT NULL,
	[Created] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedBy] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[ScheduleID] [int] NOT NULL,
	[ConfigFile] [nvarchar](250) NOT NULL,
	[Library] [nvarchar](250) NOT NULL,
	[ClassName] [nvarchar](250) NOT NULL,
	[RunOrder] [int] NOT NULL,
	[XmlParameters] [nvarchar](max) NULL,
	[RunInPrivateDomain] [bit] NOT NULL,
 CONSTRAINT [PK_tblScheduleStep] PRIMARY KEY CLUSTERED 
(
	[ScheduleStepID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [SCH].[ScheduleStepParameter]    Script Date: 18/02/2023 12:05:55 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SCH].[ScheduleStepParameter](
	[ScheduleStepParameterID] [int] IDENTITY(1,1) NOT NULL,
	[Created] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedBy] [nvarchar](50) NOT NULL,
	[ScheduleStepID] [int] NOT NULL,
	[ParameterKey] [nvarchar](50) NOT NULL,
	[ParameterValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_tblScheduleStepParameter] PRIMARY KEY CLUSTERED 
(
	[ScheduleStepParameterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [SCH].[StatusLog]    Script Date: 18/02/2023 12:05:55 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SCH].[StatusLog](
	[StatusLogID] [int] IDENTITY(1,1) NOT NULL,
	[Created] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedBy] [nvarchar](50) NOT NULL,
	[InstanceID] [int] NOT NULL,
	[ScheduleID] [int] NULL,
	[Severity] [int] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[LogType] [int] NULL,
 CONSTRAINT [PK_StatusLog] PRIMARY KEY CLUSTERED 
(
	[StatusLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [SCH].[Frequency] ADD  CONSTRAINT [DF_tblFrequency_Created]  DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [SCH].[Frequency] ADD  CONSTRAINT [DF_tblFrequency_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO

ALTER TABLE [SCH].[Instance] ADD  CONSTRAINT [DF_tblInstance_Created]  DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [SCH].[Instance] ADD  CONSTRAINT [DF_tblInstance_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO

ALTER TABLE [SCH].[Instance] ADD  CONSTRAINT [DF_Instance_ServiceName]  DEFAULT ('Sandfield.Scheduling.Service') FOR [ServiceName]
GO

ALTER TABLE [SCH].[PerformanceLog] ADD  CONSTRAINT [DF_PerformanceLog_Created]  DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [SCH].[PerformanceLog] ADD  CONSTRAINT [DF_PerformanceLog_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO

ALTER TABLE [SCH].[Schedule] ADD  CONSTRAINT [DF_tblSchedule_Created]  DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [SCH].[Schedule] ADD  CONSTRAINT [DF_tblSchedule_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO

ALTER TABLE [SCH].[Schedule] ADD  CONSTRAINT [DF_Schedule_Sunday]  DEFAULT ((0)) FOR [Sunday]
GO

ALTER TABLE [SCH].[Schedule] ADD  CONSTRAINT [DF__Schedule__NextRu__216BEC9A]  DEFAULT (NULL) FOR [NextRunTime]
GO

ALTER TABLE [SCH].[Schedule] ADD  CONSTRAINT [DF_Schedule_Status]  DEFAULT (N'Active') FOR [Status]
GO

ALTER TABLE [SCH].[Schedule] ADD  CONSTRAINT [DF_Schedule_EnableDebug]  DEFAULT ((0)) FOR [EnableDebug]
GO

ALTER TABLE [SCH].[ScheduleGroup] ADD  CONSTRAINT [DF_tblScheduleGroup_Created]  DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [SCH].[ScheduleGroup] ADD  CONSTRAINT [DF_tblScheduleGroup_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO

ALTER TABLE [SCH].[ScheduleGroup] ADD  CONSTRAINT [DF_ScheduleGroup_Active]  DEFAULT ('Active') FOR [Status]
GO

ALTER TABLE [SCH].[ScheduleStep] ADD  CONSTRAINT [DF_tblScheduleStep_Created]  DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [SCH].[ScheduleStep] ADD  CONSTRAINT [DF_tblScheduleStep_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO

ALTER TABLE [SCH].[ScheduleStep] ADD  DEFAULT ((0)) FOR [RunInPrivateDomain]
GO

ALTER TABLE [SCH].[ScheduleStepParameter] ADD  CONSTRAINT [DF_tblScheduleStepParameter_Created]  DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [SCH].[ScheduleStepParameter] ADD  CONSTRAINT [DF_tblScheduleStepParameter_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO

ALTER TABLE [SCH].[StatusLog] ADD  CONSTRAINT [DF_StatusLog_Created]  DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [SCH].[StatusLog] ADD  CONSTRAINT [DF_StatusLog_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO

ALTER TABLE [SCH].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_ScheduleGroup] FOREIGN KEY([ScheduleGroupID])
REFERENCES [SCH].[ScheduleGroup] ([ScheduleGroupID])
GO

ALTER TABLE [SCH].[Schedule] CHECK CONSTRAINT [FK_Schedule_ScheduleGroup]
GO

ALTER TABLE [SCH].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_tblSchedule_tblFrequency] FOREIGN KEY([FrequencyID])
REFERENCES [SCH].[Frequency] ([FrequencyID])
GO

ALTER TABLE [SCH].[Schedule] CHECK CONSTRAINT [FK_tblSchedule_tblFrequency]
GO

ALTER TABLE [SCH].[ScheduleStep]  WITH CHECK ADD  CONSTRAINT [FK_tblScheduleStep_tblSchedule] FOREIGN KEY([ScheduleID])
REFERENCES [SCH].[Schedule] ([ScheduleID])
GO

ALTER TABLE [SCH].[ScheduleStep] CHECK CONSTRAINT [FK_tblScheduleStep_tblSchedule]
GO

ALTER TABLE [SCH].[ScheduleStepParameter]  WITH CHECK ADD  CONSTRAINT [FK_tblScheduleStepParameter_tblScheduleStep] FOREIGN KEY([ScheduleStepID])
REFERENCES [SCH].[ScheduleStep] ([ScheduleStepID])
GO

ALTER TABLE [SCH].[ScheduleStepParameter] CHECK CONSTRAINT [FK_tblScheduleStepParameter_tblScheduleStep]
GO

ALTER TABLE [SCH].[StatusLog]  WITH NOCHECK ADD  CONSTRAINT [FK_StatusLog_Instance] FOREIGN KEY([InstanceID])
REFERENCES [SCH].[Instance] ([InstanceID])
GO

ALTER TABLE [SCH].[StatusLog] CHECK CONSTRAINT [FK_StatusLog_Instance]
GO

ALTER TABLE [SCH].[StatusLog]  WITH NOCHECK ADD  CONSTRAINT [FK_StatusLog_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [SCH].[Schedule] ([ScheduleID])
GO

ALTER TABLE [SCH].[StatusLog] CHECK CONSTRAINT [FK_StatusLog_Schedule]
GO
