USE SALMessaging_Cloud
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [WKW].[CSV_ToTable]
	(
		@CSVList nvarchar(4000)
	)
RETURNS @TableIds TABLE (TableValue nvarchar(100)) 	
AS
BEGIN
	DECLARE @TableValue varchar(100)
	DECLARE @Pos int
	
	SET @CSVList = LTRIM(RTRIM(@CSVList))+ ','
	SET @Pos = CHARINDEX(',', @CSVList, 1)
	IF REPLACE(@CSVList, N',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @TableValue = LTRIM(RTRIM(LEFT(@CSVList, @Pos - 1)))
			IF @TableValue <> ''
			BEGIN							
				INSERT INTO @TableIds (TableValue) Values (@TableValue)
			END
			SET @CSVList = RIGHT(@CSVList, LEN(@CSVList) - @Pos)
			SET @Pos = CHARINDEX(',', @CSVList, 1)
	
		END
	END	
RETURN
END
GO



/****** Object:  StoredProcedure [SCH].[DeliveryTypeDetail_Insert]    Script Date: 18/02/2023 12:12:14 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[DeliveryTypeDetail_Insert] AS SET NOCOUNT ON
GO

/****** Object:  StoredProcedure [SCH].[ExternalSystems]    Script Date: 18/02/2023 12:12:14 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ExternalSystems]
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT SCH.ExternalSystem.*
	FROM SCH.ExternalSystem 

END
GO

/****** Object:  StoredProcedure [SCH].[Instance_Select]    Script Date: 18/02/2023 12:12:15 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Instance_Select]
	@InstanceID int
AS

IF @InstanceID < 0
	BEGIN
		SELECT * FROM [SCH].[Instance] ORDER BY InstanceID DESC
	END
ELSE
	BEGIN
		SELECT * FROM [SCH].[Instance] WHERE InstanceID = @InstanceID
	END
GO

/****** Object:  StoredProcedure [SCH].[Lookup_ExternalSystem]    Script Date: 18/02/2023 12:12:15 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[Lookup_ExternalSystem] 
	@Value nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		ScheduleExternalSystemID, 
		ExternalSystem.Name
	FROM SCH.ExternalSystem ExternalSystem
	WHERE 
		(@Value IS NULL OR [Name] LIKE N'%' + @Value + N'%' OR [Name] LIKE N'%' + @Value + N'%')
	ORDER BY [Name]

RETURN
GO

/****** Object:  StoredProcedure [SCH].[Lookup_Frequency]    Script Date: 18/02/2023 12:12:15 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[Lookup_Frequency]
	@Value nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT FrequencyID, Frequency.Name
	FROM SCH.Frequency
	WHERE 
		(@Value IS NULL OR Frequency.Name LIKE N'%' + @Value + N'%' OR Frequency.Name LIKE N'%' + @Value + N'%')
	ORDER BY Frequency.Name

RETURN

GO

/****** Object:  StoredProcedure [SCH].[Lookup_Instance]    Script Date: 18/02/2023 12:12:15 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Lookup_Instance]
	@Value nvarchar(50)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT InstanceID, Instance.[Server] AS Name
	FROM SCH.Instance
	WHERE 
		(@Value IS NULL OR Instance.[Server] LIKE N'%' + @Value + N'%') AND
		LEN(ServiceName) > 0 AND
		[Enabled] = 1

	ORDER BY Instance.[Server]
RETURN
GO

/****** Object:  StoredProcedure [SCH].[Lookup_Schedule]    Script Date: 18/02/2023 12:12:15 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Lookup_Schedule]
	@Value nvarchar(50),
	--@Entity nvarchar(MAX)
	@EntityID int
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
--SELECT TableValue AS EntityCode
--INTO #EntityCodes
--FROM [WKW].CSV_ToTable(@Entity)
	SELECT 
		ScheduleID,	
		Schedule.Name + ' - ' + CONVERT(nvarchar(50),Schedule.ScheduleFrequency) + ' ' + Frequency.Name [Name]
	FROM SCH.ScheduleGroup
		LEFT JOIN SCH.Schedule ON SCH.ScheduleGroup.ScheduleGroupID = SCH.Schedule.ScheduleGroupID
		LEFT JOIN SCH.Frequency ON Schedule.FrequencyID = Frequency.FrequencyID
	WHERE  
		(@EntityID IS NULL OR SCH.ScheduleGroup.EntityID = @EntityID) AND
		--(@Entity IS NULL OR SCH.ScheduleGroup.EntityID IN (SELECT EntityID FROM SALCOR.Entity WHERE EntityCode IN (SELECT EntityCode FROM #EntityCodes))) AND
		(@Value IS NULL OR Schedule.Name LIKE N'%' + @Value + N'%') AND
		SCH.Schedule.Active = 1 --AND
		--(@EntityID IS NULL OR SCH.Schedule.EntityID = @EntityID) -- OR SCH.Schedule.EntityID IS NULL)
	ORDER BY Schedule.Name
RETURN
GO

/****** Object:  StoredProcedure [SCH].[Lookup_ScheduleAll]    Script Date: 18/02/2023 12:12:15 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Lookup_ScheduleAll]
	@Value nvarchar(50),
	@Entity nvarchar(MAX)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT TableValue AS EntityCode
INTO #EntityCodes
FROM [WKW].CSV_ToTable(@Entity)
	SELECT ScheduleID, Schedule.Name
	FROM SCH.Schedule
	WHERE 
		(@Value IS NULL OR Schedule.Name LIKE N'%' + @Value + N'%') AND
		--(@EntityID IS NULL OR SCH.Schedule.EntityID = @EntityID OR SCH.Schedule.EntityID IS NULL)
		(@Entity IS NULL OR SCH.Schedule.EntityID IS NULL OR SCH.Schedule.EntityID IN (SELECT EntityID FROM SALCOR.Entity WHERE EntityCode IN (SELECT EntityCode FROM #EntityCodes)) )
	ORDER BY Schedule.Name
RETURN
GO

/****** Object:  StoredProcedure [SCH].[Lookup_ScheduleGroup]    Script Date: 18/02/2023 12:12:15 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Lookup_ScheduleGroup]
	@Value nvarchar(50),
	@Entity nvarchar(MAX)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT TableValue AS EntityCode
	INTO #EntityCodes
	FROM [WKW].CSV_ToTable(@Entity)
		SELECT ScheduleGroupID, ScheduleGroup.Name
		FROM SCH.ScheduleGroup
		WHERE 
			(@Entity IS NULL OR SCH.ScheduleGroup.EntityID IN (SELECT EntityID FROM SALCOR.Entity WHERE EntityCode IN (SELECT EntityCode FROM #EntityCodes))) AND
			(@Value IS NULL OR ScheduleGroup.Name LIKE N'%' + @Value + N'%')
		ORDER BY ScheduleGroup.Name
RETURN
GO

/****** Object:  StoredProcedure [SCH].[PerformanceLog_Insert]    Script Date: 18/02/2023 12:12:15 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[PerformanceLog_Insert]
	@Cpu int,
	@Memory int,
	@InstanceID int
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	INSERT INTO SCH.PerformanceLog(CreatedBy, LastModifiedBy, Cpu, Memory, InstanceID)
	VALUES('Sandfield.Scheduling', 'Sandfield.Scheduling', @Cpu, @Memory, @InstanceID)
RETURN
GO

/****** Object:  StoredProcedure [SCH].[Schedule_CheckUniqueness]    Script Date: 18/02/2023 12:12:16 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_CheckUniqueness]
	@ScheduleID int,
	@Name nvarchar(500),
	@Message nvarchar(4000) OUTPUT 
AS

SET NOCOUNT ON

DECLARE @Count int
SET @Message = N''

SELECT @Count = COUNT(*) FROM SCH.Schedule
WHERE Name = @Name
	AND (ScheduleID <> @ScheduleID OR @ScheduleID IS NULL)

IF @Count > 0 BEGIN
	SET @Message = @Message + 'A Schedule already exists with the  Name ''' + @Name +  N'''.' + CHAR(13) + CHAR(10)
--	SET @Message = @Message + WKW.WorkWithObjectValue(N'Schedule', N'Article') + N' '
--	+ WKW.WorkWithObjectValue(N'Schedule', N'Name')
--	+ N' already exists with the  Name ''' + @Name +  N'''.' + CHAR(13) + CHAR(10)
END
RETURN

GRANT EXEC ON [SCH].[Schedule_CheckUniqueness] TO [MessagingWorkWith]
GO

/****** Object:  StoredProcedure [SCH].[Schedule_Cleanup]    Script Date: 18/02/2023 12:12:16 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Cleanup]
AS
DECLARE @Purge bit
DECLARE @DeleteScheduleCount int
DECLARE @DeletedScheduleID int
	
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
SELECT @DeleteScheduleCount = (
	SELECT COUNT(*) 
	FROM SCH.Schedule 
	WHERE [Status] = 'Deleted'
)
	
WHILE @DeleteScheduleCount > 0 
BEGIN
	--GET THE SCHEDULE TO BE DELETED
	SELECT @DeletedScheduleID = (
		SELECT TOP 1 ScheduleID 
		FROM SCH.Schedule 
		WHERE [Status] = 'Deleted'
	)
			
	SET @Purge = 1
	--DELETE THE LOG ENTRIES	
	WHILE @Purge = 1 BEGIN
		DELETE TOP (5000) 
		FROM SCH.StatusLog 
		WHERE ScheduleID = @DeletedScheduleID
		IF @@ROWCOUNT = 0 
			SET @Purge = 0
			WAITFOR DELAY '00:00:02'
	END
				
	--DELETE THE Step Parameters
	DELETE SCH.ScheduleStepParameter
	FROM SCH.ScheduleStepParameter
		INNER JOIN SCH.ScheduleStep ON SCH.ScheduleStepParameter.ScheduleStepID = SCH.ScheduleStep.ScheduleStepID
	WHERE SCH.ScheduleStep.ScheduleID = @DeletedScheduleID
			
	--DELETE THE STEPS
	DELETE 
	FROM SCH.ScheduleStep
	WHERE SCH.ScheduleStep.ScheduleID = @DeletedScheduleID
	--DELETE THE SCHEDULE
	DELETE 
	FROM SCH.Schedule
	WHERE SCH.Schedule.ScheduleID = @DeletedScheduleID
		
	--CHECK IF THERE ARE MORE DELETED SCHEDULES
	SELECT @DeleteScheduleCount = (
		SELECT COUNT(*) 
		FROM SCH.Schedule 
		WHERE [Status] = 'Deleted'
	)
		
	WAITFOR DELAY '00:00:02'
END
-- Now that all Schedules should be gone, delete any deleted Schedule Groups
DELETE
FROM [SCH].[ScheduleGroup]
WHERE [Status] = 'Deleted'
RETURN

GO

/****** Object:  StoredProcedure [SCH].[Schedule_Delete]    Script Date: 18/02/2023 12:12:16 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Delete]
	@ScheduleID int
AS

SET NOCOUNT ON

SET XACT_ABORT ON
BEGIN TRANSACTION

		UPDATE SCH.Schedule
		SET [Status] = 'Deleted', Active = 0, Name = Name + ' - Deleted'
		WHERE SCH.Schedule.ScheduleID = @ScheduleID
		


COMMIT TRANSACTION
SET XACT_ABORT OFF
RETURN

GO

/****** Object:  StoredProcedure [SCH].[Schedule_Export]    Script Date: 18/02/2023 12:12:16 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Export] 
(
	@ScheduleID int,
	@XML		nvarchar(max)OUTPUT
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @XML = 
(
	SELECT 
		XmlSchedule.Name,
		XmlSchedule.Description,
		XmlSchedule.FrequencyID,
		XmlSchedule.Active,
		XmlSchedule.StartDate,
		XmlSchedule.EndDate,
		XmlSchedule.ScheduleFrequency,
		XmlSchedule.EveryNumberOfWeeks,
		XmlSchedule.Monday,
		XmlSchedule.Tuesday,
		XmlSchedule.Wednesday,
		XmlSchedule.Thursday,
		XmlSchedule.Friday,
		XmlSchedule.Saturday,
		XmlSchedule.Sunday,
		XmlSchedule.LastRunCompleted,
		XmlSchedule.ScheduleGroupID,
		ScheduleGroup.Name AS ScheduleGroupName,
		Frequency.Name AS Frequency,
		(
			SELECT
			  XmlScheduleStep.Name,
			  XmlScheduleStep.ScheduleID,
			  XmlScheduleStep.ConfigFile,
			  XmlScheduleStep.Library,
			  XmlScheduleStep.ClassName,
			  XmlScheduleStep.RunOrder,
			  XmlScheduleStep.XmlParameters,
			  (
					SELECT
					XmlScheduleStepParameters.ParameterKey AS 'ParamKey',
					'ParamValue' + CAST(XmlScheduleStepParameters.ScheduleStepParameterID as varchar(50)) AS 'ParamValue'
					FROM SCH.ScheduleStepParameter XmlScheduleStepParameters
					WHERE XmlScheduleStep.ScheduleStepID = XmlScheduleStepParameters.ScheduleStepID
					FOR XML PATH('Parameter') , type
				) AS 'Parameters'
			FROM SCH.ScheduleStep XmlScheduleStep
			WHERE XmlScheduleStep.ScheduleID = XmlSchedule.ScheduleID
			FOR XML RAW('Step') , type
		) AS 'Steps'
	FROM SCH.Schedule XmlSchedule 
		LEFT JOIN SCH.ScheduleGroup ON XmlSchedule.ScheduleGroupID = SCH.ScheduleGroup.ScheduleGroupID
		INNER JOIN SCH.Frequency ON XmlSchedule.FrequencyID = Frequency.FrequencyID
		WHERE XmlSchedule.ScheduleID = @ScheduleID
	FOR XML RAW('Schedule')
)

DECLARE @ParamValue nvarchar(max)
DECLARE @ScheduleStepParameterID int
DECLARE @KeyName varchar(50)

DECLARE ScheduleStepParameter_Cursor CURSOR LOCAL READ_ONLY FAST_FORWARD FOR
	SELECT
		SCH.ScheduleStepParameter.ScheduleStepParameterID,
		'<![CDATA[' + SCH.ScheduleStepParameter.ParameterValue + ']]>'
	FROM SCH.ScheduleStep
	INNER JOIN SCH.ScheduleStepParameter ON SCH.ScheduleStep.ScheduleStepID = SCH.ScheduleStepParameter.ScheduleStepID
	WHERE SCH.ScheduleStep.ScheduleID = @ScheduleID
	            
OPEN ScheduleStepParameter_Cursor
FETCH NEXT FROM ScheduleStepParameter_Cursor INTO @ScheduleStepParameterID, @ParamValue

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @KeyName = 'ParamValue' + CAST(@ScheduleStepParameterID as varchar(50))
	SET @XML = REPLACE(@XML, @KeyName, @ParamValue)
FETCH NEXT FROM ScheduleStepParameter_Cursor INTO @ScheduleStepParameterID, @ParamValue
END

CLOSE ScheduleStepParameter_Cursor
DEALLOCATE ScheduleStepParameter_Cursor
GO


/****** Object:  StoredProcedure [SCH].[Schedule_LastModified]    Script Date: 18/02/2023 12:12:16 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_LastModified]
	@LastModified datetime
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT 
		sch.Schedule.ScheduleID,
		sch.Schedule.Created,
		sch.Schedule.CreatedBy,
		sch.Schedule.LastModified,
		sch.Schedule.LastModifiedBy,
		sch.Schedule.Name,
		sch.Schedule.Description,
		sch.Schedule.FrequencyID,
		sch.Schedule.Active,
		sch.Schedule.StartDate,
		sch.Schedule.EndDate,
		sch.Schedule.ScheduleFrequency,
		sch.Schedule.EveryNumberOfWeeks,
		sch.Schedule.Monday,
		sch.Schedule.Tuesday,
		sch.Schedule.Wednesday,
		sch.Schedule.Thursday,
		sch.Schedule.Friday,
		sch.Schedule.Saturday,
		sch.Schedule.Sunday,
		sch.Schedule.LastRunCompleted,
		sch.Schedule.ScheduleGroupID,
		sch.Schedule.NextRunTime,
		sch.Frequency.Name AS FrequencyName,
		sch.Frequency.SystemName AS FrequencySystemName,
		sch.Instance.[Server] AS Instance,
		SCH.Schedule.RunBetweenStartTime,
		SCH.Schedule.RunBetweenEndTime,
		SCH.Schedule.DelayBetweenSteps,
		SCH.Schedule.EnableDebug
	FROM sch.Schedule
		INNER JOIN sch.Frequency ON sch.Schedule.FrequencyID = sch.Frequency.FrequencyID
		INNER JOIN sch.Instance ON SCH.Schedule.InstanceID = sch.Instance.InstanceID
	WHERE 
		sch.Schedule.LastModified > @LastModified OR
		(
			(sch.Schedule.LastRunCompleted < DATEADD(minute, 60, GETDATE()) AND sch.Frequency.SystemName = 'Minutely') AND
			(sch.Schedule.LastRunCompleted < DATEADD(minute, 15, GETDATE()) AND sch.Frequency.SystemName = 'Secondly') AND
			sch.Schedule.Active = 1 AND
			sch.Schedule.EndDate > GETDATE()
		)
END
GO

/****** Object:  StoredProcedure [SCH].[Schedule_LastModified_XML]    Script Date: 18/02/2023 12:12:17 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_LastModified_XML]
	@LastModified datetime,
	@SchedulesXML xml OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	--;WITH XMLNAMESPACES ('http://james.newtonking.com/projects/json' AS json)
	SELECT @SchedulesXML = (
	SELECT (
		SELECT SCH.Schedule.[ScheduleID] AS '@scheduleID', 
			SCH.Schedule.[Name] AS '@scheduleName', 
			SCH.Frequency.SystemName AS '@frequencySystemName',
			[ScheduleFrequency] AS '@scheduleFrequency', 
			SCH.Instance.[Server] AS '@instance',
			[Active] AS '@active', 
			[StartDate] AS '@startDate', 
			[EndDate] AS '@endDate', 
			ISNULL([EveryNumberOfWeeks], 1) AS '@everyNumberOfWeeks', 
			ISNULL([Monday], 0) AS '@monday', 
			ISNULL([Tuesday], 0)  AS '@tuesday', 
			ISNULL([Wednesday], 0) AS '@wednesday', 
			ISNULL([Thursday], 0) AS '@thursday', 
			ISNULL([Friday], 0) AS '@friday', 
			ISNULL([Saturday], 0) AS '@saturday', 
			ISNULL([Sunday], 0) AS '@sunday', 
			ISNULL([EnableDebug], 0) AS '@enableDebug',
			[RunBetweenStartTime] AS '@runBetweenStartTime', 
			[RunBetweenEndTime] AS '@runBetweenEndTime', 
			ISNULL([DelayBetweenSteps], 0) AS '@stepRunDelay', 
			(
					SELECT --'true' AS '@json:Array',
						[ScheduleStepID] AS '@scheduleStepID', 
						--[Name] AS '@name', 
						[ConfigFile] AS '@configFile', 
						[Library] AS '@library', 
						[ClassName] AS '@className', 
						[RunOrder] AS '@runOrder', 
						[RunInPrivateDomain] AS '@runInPrivateDomain',
						ROW_NUMBER() OVER (ORDER BY RunOrder) AS '@stepNumber',
						(
							SELECT DISTINCT tblExternalSystem.[ActionAssembly]
							FROM SCH.ScheduleStepParameter
								LEFT JOIN tblDeliveryTypeDetail ON ScheduleStepParameter.ParameterValue = tblDeliveryTypeDetail.[Name]
								LEFT JOIN tblExternalSystem ON tblDeliveryTypeDetail.ExternalSystemID = tblExternalSystem.ExternalSystemID
							WHERE ScheduleStepID = SCH.ScheduleStep.ScheduleStepID
						) AS '@domainInfo',
						[XmlParameters] AS xmlParameters, 
						(
							SELECT --'true' AS '@json:Array',
								[ParameterKey] AS '@key',
								ParameterValue AS '@value'
							FROM SCH.ScheduleStepParameter
							WHERE ScheduleStepID = SCH.ScheduleStep.ScheduleStepID
							FOR XML PATH ('parameter'), TYPE
						)
					FROM SCH.ScheduleStep
					WHERE ScheduleStep.ScheduleID = SCh.Schedule.ScheduleID
					FOR XML PATH ('scheduleStep'), TYPE
			)
		FROM SCH.Schedule 
			INNER JOIN SCH.Frequency ON SCH.Schedule.FrequencyID = SCH.Frequency.FrequencyID
			INNER JOIN SCH.Instance ON SCH.Schedule.InstanceID = SCH.Instance.InstanceID
		WHERE SCH.Schedule.LastModified > @LastModified OR
			(
				(SCH.Schedule.LastRunCompleted < DATEADD(minute, 60, GETDATE()) AND sch.Frequency.SystemName = 'Minutely') AND
				(SCH.Schedule.LastRunCompleted < DATEADD(minute, 15, GETDATE()) AND sch.Frequency.SystemName = 'Secondly') AND
				SCH.Schedule.Active = 1 AND
				SCH.Schedule.EndDate > GETDATE()
			)
		FOR XML PATH ('schedule'), TYPE
	)  FOR XML PATH ('schedules'), TYPE
	)
END
GO

/****** Object:  StoredProcedure [SCH].[Schedule_LastRunCompleted_BulkUpdate]    Script Date: 18/02/2023 12:12:17 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_LastRunCompleted_BulkUpdate]
	@ScheduleXml xml
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--DECLARE @ScheduleXml xml
--SET @ScheduleXml = CONVERT(xml, N'<Schedules><Schedule ScheduleID="1" LastRunTime="2021-08-17 12:12:00" /><Schedule ScheduleID="2" LastRunTime="2021-08-18 12:12:00" /></Schedules>')

SELECT
	T.c.value('(@ScheduleID)[1]', 'int') AS ScheduleID,
	T.c.value('(@LastRunTime)[1]', 'datetime') AS LastRunTime
INTO #LastRunTimes
FROM
	@ScheduleXml.nodes('/Schedules/Schedule') T(c)

UPDATE SCH.Schedule
	SET SCH.Schedule.LastRunCompleted = #LastRunTimes.LastRunTime
FROM SCH.Schedule WITH (INDEX=PK_tblSchedule)
	INNER JOIN #LastRunTimes ON SCH.Schedule.ScheduleID = #LastRunTimes.ScheduleID
WHERE YEAR(#LastRunTimes.LastRunTime) < 9999  --EXCLUDE WHERE LAST RUN TIME IS DATETIME.MAXVALUE

DROP TABLE #LastRunTimes

RETURN
GO

/****** Object:  StoredProcedure [SCH].[Schedule_LastRunCompleted_Upadte]    Script Date: 18/02/2023 12:12:17 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[Schedule_LastRunCompleted_Upadte]
	@ScheduleID int,
	@LastRunCompleted datetime 
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE
		SCH.Schedule
	SET 
		SCH.Schedule.LastRunCompleted = @LastRunCompleted
	WHERE
		ScheduleID = @ScheduleID

RETURN

GO

/****** Object:  StoredProcedure [SCH].[Schedule_LastRunCompleted_Update]    Script Date: 18/02/2023 12:12:17 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[Schedule_LastRunCompleted_Update]
	@ScheduleID int,
	@LastRunCompleted datetime 
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE
		SCH.Schedule
	SET 
		SCH.Schedule.LastRunCompleted = @LastRunCompleted
	WHERE
		ScheduleID = @ScheduleID

RETURN
GO

/****** Object:  StoredProcedure [SCH].[Schedule_Logs]    Script Date: 18/02/2023 12:12:17 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Logs]
	@ScheduleID Int
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT TOP 100
		StatusLogID,
		Created,
		CASE 
			WHEN StatusLog.Severity = 1 THEN 'Error'
			WHEN StatusLog.Severity = 2 THEN 'Chatter'
			WHEN StatusLog.Severity = 3 THEN 'Verbose'
		END AS Severity,
		[Message]
	FROM SCH.StatusLog
	WHERE ScheduleID = @ScheduleID
	ORDER BY StatusLogID DESC	
RETURN
GO

/****** Object:  StoredProcedure [SCH].[Schedule_Logs_Cleanup]    Script Date: 18/02/2023 12:12:17 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Logs_Cleanup]
AS

	DECLARE @OldLogCount int
	
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

				--DELETE THE LOG ENTRIES	
				SELECT @OldLogCount = (SELECT COUNT(*) FROM SCH.StatusLog WHERE Created < DATEADD(DAY,-7,GETDATE()))
				WHILE @OldLogCount > 0 BEGIN
					DELETE TOP (5000) FROM SCH.StatusLog WHERE Created < DATEADD(DAY,-7,GETDATE())
					SELECT @OldLogCount = (SELECT COUNT(*) FROM SCH.StatusLog WHERE Created < DATEADD(DAY,-7,GETDATE()))
					WAITFOR DELAY '00:00:02'
				END
				
				


GO

/****** Object:  StoredProcedure [SCH].[Schedule_NextRunCompleted_BulkUpdate]    Script Date: 18/02/2023 12:12:17 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_NextRunCompleted_BulkUpdate]
	@ScheduleXml xml
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--DECLARE @ScheduleXml xml
--SET @ScheduleXml = CONVERT(xml, N'<Schedules><Schedule ScheduleID="1" NextRunTime="2021-08-17 12:12:00" /><Schedule ScheduleID="2" NextRunTime="2021-08-18 12:12:00" /></Schedules>')

SELECT
	T.c.value('(@ScheduleID)[1]', 'int') AS ScheduleID,
	T.c.value('(@NextRunTime)[1]', 'datetime') AS NextRunTime
INTO #NextRunTimes
FROM
	@ScheduleXml.nodes('/Schedules/Schedule') T(c)

UPDATE SCH.Schedule
	SET SCH.Schedule.NextRunTime = #NextRunTimes.NextRunTime
FROM SCH.Schedule WITH (INDEX=PK_tblSchedule)
	INNER JOIN #NextRunTimes ON SCH.Schedule.ScheduleID = #NextRunTimes.ScheduleID
WHERE YEAR(#NextRunTimes.NextRunTime) < 9999  --EXCLUDE WHERE NEXT RUN TIME IS DATETIME.MAXVALUE

DROP TABLE #NextRunTimes

RETURN
GO

/****** Object:  StoredProcedure [SCH].[Schedule_NextRunTime_Upadte]    Script Date: 18/02/2023 12:12:17 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [SCH].[Schedule_NextRunTime_Upadte]
	@ScheduleID int,
	@NextRunTime datetime
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @PreviousNextRunTime datetime

	IF YEAR(@NextRunTime) = 9999 BEGIN
		SET @NextRunTime = NULL
	END

	SELECT @PreviousNextRunTime = NextRunTime
	FROM SCH.Schedule
	WHERE ScheduleID = @ScheduleID

	IF @PreviousNextRunTime <> @NextRunTime BEGIN
		UPDATE
			SCH.Schedule
		SET 
			SCH.Schedule.NextRunTime = @NextRunTime
		WHERE
			ScheduleID = @ScheduleID
	END 

RETURN

GO

/****** Object:  StoredProcedure [SCH].[Schedule_NextRunTime_Update]    Script Date: 18/02/2023 12:12:18 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[Schedule_NextRunTime_Update]
	@ScheduleID int,
	@NextRunTime datetime
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @PreviousNextRunTime datetime

	IF YEAR(@NextRunTime) = 9999 BEGIN
		SET @NextRunTime = NULL
	END

	SELECT @PreviousNextRunTime = NextRunTime
	FROM SCH.Schedule
	WHERE ScheduleID = @ScheduleID


	IF (@PreviousNextRunTime IS NULL AND @NextRunTime IS NOT NULL) OR (@PreviousNextRunTime <> @NextRunTime) BEGIN
		UPDATE
			SCH.Schedule
		SET 
			SCH.Schedule.NextRunTime = @NextRunTime
		WHERE
			ScheduleID = @ScheduleID
	END 

RETURN
GO

/****** Object:  StoredProcedure [SCH].[Schedule_Select]    Script Date: 18/02/2023 12:12:18 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Select]
	@ScheduleID int
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT
	SCH.Schedule.[Name],
	[Description],
	SCH.ScheduleGroup.ScheduleGroupID,
	SCH.ScheduleGroup.Name AS ScheduleGroup,
	SCH.Frequency.FrequencyID,
	SCH.Frequency.Name AS Frequency,
	Active,
	RestartOnThreshHold,
	CONVERT(nvarchar, StartDate, 106) AS StartDate,
	CONVERT(nvarchar, StartDate, 108) AS StartTime,
	CONVERT(nvarchar, EndDate, 106) AS EndDate,
	CONVERT(nvarchar, EndDate, 108) AS EndTime,
	ScheduleFrequency,
	EveryNumberOfWeeks,
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday,
	SCH.Schedule.Created,
	SCH.Schedule.CreatedBy,
	SCH.Schedule.LastModified,
	SCH.Schedule.LastModifiedBy,
	SCH.Schedule.LastRunCompleted,
	SCH.Schedule.NextRunTime,
	SCH.Instance.InstanceID,
	SCH.Instance.[Server] AS Instance,
	SCH.Schedule.RunBetweenStartTime,
	SCH.Schedule.RunBetweenEndTime,
	SCH.Schedule.DelayBetweenSteps,
	ISNULL(SCH.Schedule.EnableDebug, 0) AS EnableDebug
FROM SCH.Schedule
	INNEr JOIN SCH.Instance ON SCH.Schedule.InstanceID = SCH.Instance.InstanceID
	INNER JOIN SCH.Frequency ON SCH.Schedule.FrequencyID = SCH.Frequency.FrequencyID
	LEFT JOIN SCH.ScheduleGroup ON SCH.ScheduleGroup.ScheduleGroupID = SCH.Schedule.ScheduleGroupID  
WHERE
	ScheduleID = @ScheduleID AND 
	Schedule.[Status] = 'Active'

GO

/****** Object:  StoredProcedure [SCH].[Schedule_Steps]    Script Date: 18/02/2023 12:12:18 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Steps]
	@ScheduleID int
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT 
		ScheduleStep.ScheduleStepID,
		ScheduleStepParameter.ScheduleStepParameterID,
		ScheduleStep.Name,
		ScheduleStep.Library,
		ScheduleStep.ClassName,
		ScheduleStep.RunOrder,
		ISNULL(ScheduleStepParameter.ParameterKey, '') AS ParameterKey,
		ISNULL(ScheduleStepParameter.ParameterValue, '') AS ParameterValue,
		ScheduleStep.ConfigFile,
		ISNULL(ScheduleStep.XmlParameters, '') AS XmlParameters,
		ScheduleStep.RunInPrivateDomain
	FROM SCH.ScheduleStep
		LEFT JOIN SCH.ScheduleStepParameter ON ScheduleStep.ScheduleStepID = ScheduleStepParameter.ScheduleStepID
	WHERE ScheduleStep.ScheduleID = @ScheduleID
	ORDER BY RunOrder
RETURN
GO

/****** Object:  StoredProcedure [SCH].[Schedule_Update]    Script Date: 18/02/2023 12:12:18 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Update]
	@Name nvarchar(500),
	@InstanceID int,
	@ScheduleGroupID int,
	@Description nvarchar(2000),
	@FrequencyID int,
	@Active bit,
	@RestartOnThreshHold bit,
	@StartDate datetime,
	@EndDate datetime,
	@RunBetweenStartTime datetime,
	@RunBetweenEndTime datetime,
	@ScheduleFrequency smallint,
	@DelayBetweenSteps smallint,
	@EveryNumberOfWeeks smallint,
	@Monday bit,
	@Tuesday bit,
	@Wednesday bit,
	@Thursday bit,
	@Friday bit,
	@Saturday bit,
	@Sunday bit,
	@LastModifiedBy nvarchar(50),
	@EnableDebug bit = 0,
	@ScheduleID int,
	@Message nvarchar(4000) OUTPUT
AS
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRANSACTION
SET @Message = N''
EXECUTE [SCH].[Schedule_CheckUniqueness] @ScheduleID, @Name, @Message OUTPUT
IF LEN(@Message) = 0 BEGIN
	UPDATE Schedule SET
		Name = @Name,
		InstanceID = @InstanceID,
		ScheduleGroupID = @ScheduleGroupID,
		Description = @Description,
		FrequencyID = @FrequencyID,
		Active = @Active,
		RestartOnThreshHold = @RestartOnThreshHold,
		StartDate = @StartDate,
		EndDate = @EndDate,
		ScheduleFrequency = @ScheduleFrequency,
		DelayBetweenSteps = @DelayBetweenSteps,
		EveryNumberOfWeeks = @EveryNumberOfWeeks,
		Monday = @Monday,
		Tuesday = @Tuesday,
		Wednesday = @Wednesday,
		Thursday = @Thursday,
		Friday = @Friday,
		Saturday = @Saturday,
		Sunday = @Sunday,
		LastModified = GETDATE(),
		LastModifiedBy = @LastModifiedBy,
		RunBetweenStartTime = @RunBetweenStartTime,
		RunBetweenEndTime = @RunBetweenEndTime,
		EnableDebug = @EnableDebug
	WHERE ScheduleID = @ScheduleID
End
COMMIT TRANSACTION
SET XACT_ABORT OFF
GO

/****** Object:  StoredProcedure [SCH].[ScheduleByID]    Script Date: 18/02/2023 12:12:18 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ScheduleByID]-- 'PGEYSER'
	@InstanceName nvarchar(50),
	@ScheduleID int
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT SCH.Schedule.*, 
		SCH.Frequency.Name AS FrequencyName,
		SCH.Frequency.SystemName AS FrequencySystemName,
		SCH.Instance.[Server] AS Instance
	FROM SCH.Schedule 
		INNER JOIN SCH.Frequency ON SCH.Schedule.FrequencyID = SCH.Frequency.FrequencyID
		INNER JOIN SCH.Instance ON SCH.Schedule.InstanceID = SCH.Instance.InstanceID
	WHERE Active = 1 AND
		SCH.Instance.Server = @InstanceName AND
		SCH.Schedule.ScheduleID = @ScheduleID AND
		SCH.Schedule.[Status] = 'Active'

END


GO

/****** Object:  StoredProcedure [SCH].[ScheduleGroup_CheckKeyExists]    Script Date: 18/02/2023 12:12:18 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ScheduleGroup_CheckKeyExists]
	@PrimaryKey nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


SELECT ScheduleGroupID
FROM [sch].[ScheduleGroup]
WHERE ScheduleGroupID = @PrimaryKey

RETURN

GO

/****** Object:  StoredProcedure [SCH].[ScheduleGroup_CheckUniqueness]    Script Date: 18/02/2023 12:12:18 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ScheduleGroup_CheckUniqueness]
	@Name nvarchar(200),
	@ScheduleGroupID Int,
	@Message nvarchar(4000) OUTPUT 
AS

SET NOCOUNT ON

DECLARE @Count int
SET @Message = N''

SELECT @Count = COUNT(*) FROM sch.ScheduleGroup
WHERE Name = @Name
	AND (ScheduleGroupID <> @ScheduleGroupID OR @ScheduleGroupID IS NULL)

IF @Count > 0 BEGIN
	SET @Message = @Message + 'A Schedule group already exists with the  Name ''' + @Name +  N'''.' + CHAR(13) + CHAR(10)
--	SET @Message = @Message + WKW.WorkWithObjectValue(N'ScheduleGroup', N'Article') + N' '
--	+ WKW.WorkWithObjectValue(N'ScheduleGroup', N'Name')
--	+ N' already exists with the  Name ''' + @Name +  N'''.' + CHAR(13) + CHAR(10)
END
RETURN

GRANT EXEC ON [sch].[ScheduleGroup_CheckUniqueness] TO [ScheduleRole]
GO

/****** Object:  StoredProcedure [SCH].[ScheduleGroup_Delete]    Script Date: 18/02/2023 12:12:18 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ScheduleGroup_Delete]
	@ScheduleGroupID int,
	@Message nvarchar(4000) OUTPUT
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @existingSchedules int = (
	SELECT COUNT(*)
	FROM SCH.Schedule
	WHERE ScheduleGroupID = @ScheduleGroupID
		AND [Status] != 'Deleted'
)
IF @existingSchedules = 0
BEGIN
	UPDATE SCH.ScheduleGroup
	SET [Status] = 'Deleted',
		-- This is going on the front because the cleanup job is looking for
		-- Schedule Thread groups with "Schedule Threads" at the end
		LastModified = GETDATE(),
		LastModifiedBy = 'SCH.ScheduleGroup_Delete',
		[Name] = 'Deleted - ' + [Name] 
	WHERE ScheduleGroupID = @ScheduleGroupID
END
ELSE
BEGIN
	SET @Message = 'This schedule group is being used by a schedule - please update any linked schedules before deleting this schedule group.'
END	
RETURN

GO

/****** Object:  StoredProcedure [SCH].[ScheduleGroup_Insert]    Script Date: 18/02/2023 12:12:19 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[ScheduleGroup_Insert]
	@Name nvarchar(200),
	@EntityID int,
	@CreatedBy nvarchar(50),
	@ScheduleGroupID Int OUTPUT,
	@Message nvarchar(4000) OUTPUT
AS

SET NOCOUNT ON

SET XACT_ABORT ON
BEGIN TRANSACTION

SET @Message = N''

EXECUTE [sch].[ScheduleGroup_CheckUniqueness] @Name,NULL, @Message OUTPUT

IF LEN(@Message) = 0 BEGIN
	INSERT INTO sch.ScheduleGroup (
		Name,
		EntityID,
		CreatedBy,
		LastModifiedBy
	)
	VALUES (
		@Name,
		@EntityID,
		@CreatedBy,
		@CreatedBy
	)

	SET @ScheduleGroupID = SCOPE_IDENTITY()
END

COMMIT TRANSACTION
SET XACT_ABORT OFF
RETURN

GO

/****** Object:  StoredProcedure [SCH].[ScheduleGroup_Select]    Script Date: 18/02/2023 12:12:19 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[ScheduleGroup_Select]
	@ScheduleGroupID Int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		SCH.ScheduleGroup.Name,
		SCH.ScheduleGroup.EntityID,
		ISNULL(SALCOR.Entity.Name, '') AS Entity,
		SCH.ScheduleGroup.Created,
		SCH.ScheduleGroup.CreatedBy,
		SCH.ScheduleGroup.LastModified,
		SCH.ScheduleGroup.LastModifiedBy
	FROM SCH.ScheduleGroup
		LEFT OUTER JOIN SALCOR.Entity ON SCH.ScheduleGroup.EntityID = SALCOR.Entity.EntityID 
	WHERE
		ScheduleGroupID = @ScheduleGroupID

RETURN

GO

/****** Object:  StoredProcedure [SCH].[ScheduleGroup_Update]    Script Date: 18/02/2023 12:12:19 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[ScheduleGroup_Update]
	@Name nvarchar(500),
	@EntityID int,
	@LastModifiedBy nvarchar(50),
	@ScheduleGroupID Int,
	@Message nvarchar(4000) OUTPUT
AS

SET NOCOUNT ON

SET XACT_ABORT ON
BEGIN TRANSACTION

SET @Message = N''

EXECUTE [sch].[ScheduleGroup_CheckUniqueness] @Name, @ScheduleGroupID, @Message OUTPUT

IF LEN(@Message) = 0 BEGIN
	UPDATE sch.ScheduleGroup SET
		Name = @Name,
		EntityID = @EntityID,
		LastModified = GETDATE(),
		LastModifiedBy = @LastModifiedBy
	WHERE ScheduleGroupID = @ScheduleGroupID
END

COMMIT TRANSACTION
SET XACT_ABORT OFF
RETURN
GO

/****** Object:  StoredProcedure [SCH].[ScheduleGroupExternalSystem_Delete]    Script Date: 18/02/2023 12:12:19 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE  [SCH].[ScheduleGroupExternalSystem_Delete] 
	@ScheduleGroupExternalSystemID int,
	@Message nvarchar(4000) OUTPUT 
AS

SET NOCOUNT ON

SET XACT_ABORT ON
BEGIN TRANSACTION

	SET @Message = N''

	DELETE 
	FROM SCH.ScheduleGroupExternalSystem  
	WHERE
		ScheduleGroupExternalSystemID = @ScheduleGroupExternalSystemID

COMMIT TRANSACTION
SET XACT_ABORT OFF
RETURN

GO

/****** Object:  StoredProcedure [SCH].[ScheduleGroupExternalSystem_Insert]    Script Date: 18/02/2023 12:12:19 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[ScheduleGroupExternalSystem_Insert] 
	@ScheduleGroupID int,
	@ScheduleExternalSystemID int,
	@KeyValue varchar(500),
	@Message nvarchar(4000) OUTPUT 
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @Count int
	SET @Count = 0
	SET @Message = ''

	SELECT @Count = COUNT(*)
	FROM SCH.ScheduleGroupExternalSystem 
	WHERE
		ScheduleGroupID = @ScheduleGroupID
		AND ScheduleExternalSystemID = @ScheduleExternalSystemID
		AND KeyValue = @KeyValue  

	IF @Count = 0 BEGIN
		INSERT INTO [SCH].[ScheduleGroupExternalSystem] 
		(ScheduleGroupID,ScheduleExternalSystemID,KeyValue)
		VALUES
		(@ScheduleGroupID, @ScheduleExternalSystemID, @KeyValue)
	END
	ELSE BEGIN
		SET @Message = @Message + 'A schedule group external system already exists with the  key value ''' + @KeyValue +  N'''.' + CHAR(13) + CHAR(10)
	END
	
RETURN
GO

/****** Object:  StoredProcedure [SCH].[ScheduleGroupExternalSystem_Select]    Script Date: 18/02/2023 12:12:19 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[ScheduleGroupExternalSystem_Select]
	@ScheduleGroupID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	
	SELECT 
		SCH.ScheduleGroupExternalSystem.ScheduleGroupID,
		SCH.ScheduleGroupExternalSystem.ScheduleExternalSystemID,
		SCH.ScheduleGroupExternalSystem.KeyValue,
		SCH.ExternalSystem.Name AS ExternalSystemName,
		SCH.ScheduleGroupExternalSystem.ScheduleGroupExternalSystemID
	FROM 
		SCH.ScheduleGroupExternalSystem 
		INNER JOIN SCH.ExternalSystem ON SCH.ExternalSystem.ScheduleExternalSystemID = SCH.ScheduleGroupExternalSystem.ScheduleExternalSystemID
	WHERE
		SCH.ScheduleGroupExternalSystem.ScheduleGroupID = @ScheduleGroupID

RETURN
GO

/****** Object:  StoredProcedure [SCH].[ScheduleGroupExternalSystem_Update]    Script Date: 18/02/2023 12:12:19 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [SCH].[ScheduleGroupExternalSystem_Update] 
	@ScheduleGroupExternalSystemID int,
	@ScheduleGroupID int,
	@ScheduleExternalSystemID int,
	@KeyValue varchar(500),
	@Message nvarchar(4000) OUTPUT 
AS

SET NOCOUNT ON

SET XACT_ABORT ON
BEGIN TRANSACTION

SET @Message = N''

IF LEN(@Message) = 0 BEGIN

	UPDATE SCH.ScheduleGroupExternalSystem 
	SET 	
		ScheduleGroupID = @ScheduleGroupID,
		ScheduleExternalSystemID = @ScheduleExternalSystemID,
		KeyValue = @KeyValue 
	WHERE
		ScheduleGroupExternalSystemID = @ScheduleGroupExternalSystemID
	 

END

COMMIT TRANSACTION
SET XACT_ABORT OFF
RETURN


GO

/****** Object:  StoredProcedure [SCH].[Schedules]    Script Date: 18/02/2023 12:12:19 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedules]-- 'PGEYSER'
	@InstanceName nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT SCH.Schedule.*, 
		SCH.Frequency.Name AS FrequencyName,
		SCH.Frequency.SystemName AS FrequencySystemName,
		SCH.Instance.[Server] AS Instance
	FROM SCH.Schedule 
		INNER JOIN SCH.Frequency ON SCH.Schedule.FrequencyID = SCH.Frequency.FrequencyID
		INNER JOIN SCH.Instance ON SCH.Schedule.InstanceID = SCH.Instance.InstanceID
	WHERE Active = 1 AND
		SCH.Instance.Server = @InstanceName AND
		SCH.Schedule.[Status] = 'Active'

END


GO

/****** Object:  StoredProcedure [SCH].[Schedules_RunNow]    Script Date: 18/02/2023 12:12:20 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedules_RunNow]
	@ScheduleID nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT SCH.Schedule.*, 
		SCH.Frequency.Name AS FrequencyName,
		SCH.Frequency.SystemName AS FrequencySystemName
	FROM SCH.Schedule
		INNER JOIN SCH.Frequency ON SCH.Schedule.FrequencyID = SCH.Frequency.FrequencyID
		INNER JOIN SCH.Instance ON SCH.Schedule.InstanceID = SCH.Instance.InstanceID
	WHERE --Active = 1 AND
		SCH.Schedule.ScheduleID = @ScheduleID

END
GO

/****** Object:  StoredProcedure [SCH].[Schedules_XML]    Script Date: 18/02/2023 12:12:20 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedules_XML]
	@InstanceName nvarchar(50),
	@ScheduleID int = NULL,
	@SchedulesXML xml OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	--;WITH XMLNAMESPACES ('http://james.newtonking.com/projects/json' AS json)
	SELECT @SchedulesXML = (
	SELECT (
		SELECT 
			SCH.Schedule.[ScheduleID] AS '@scheduleID', 
			SCH.Schedule.[Name] AS '@scheduleName', 
			SCH.Frequency.SystemName AS '@frequencySystemName',
			[ScheduleFrequency] AS '@scheduleFrequency', 
			SCH.Instance.[Server] AS '@instance',
			[Active] AS '@active', 
			[StartDate] AS '@startDate', 
			[EndDate] AS '@endDate', 
			ISNULL([EveryNumberOfWeeks], 1) AS '@everyNumberOfWeeks', 
			ISNULL([Monday], 0) AS '@monday', 
			ISNULL([Tuesday], 0)  AS '@tuesday', 
			ISNULL([Wednesday], 0) AS '@wednesday', 
			ISNULL([Thursday], 0) AS '@thursday', 
			ISNULL([Friday], 0) AS '@friday', 
			ISNULL([Saturday], 0) AS '@saturday', 
			ISNULL([Sunday], 0) AS '@sunday', 
			ISNULL([EnableDebug], 0) AS '@enableDebug',
			[RunBetweenStartTime] AS '@runBetweenStartTime', 
			[RunBetweenEndTime] AS '@runBetweenEndTime', 
			ISNULL([DelayBetweenSteps], 0) AS '@stepRunDelay', 
			(
					SELECT --'true' AS '@json:Array',
						[ScheduleStepID] AS '@scheduleStepID', 
						--[Name] AS '@name', 
						[ConfigFile] AS '@configFile', 
						[Library] AS '@library', 
						[ClassName] AS '@className', 
						[RunOrder] AS '@runOrder', 
						[RunInPrivateDomain] AS '@runInPrivateDomain',
						ROW_NUMBER() OVER (ORDER BY RunOrder) AS '@stepNumber',
						(
							SELECT DISTINCT tblExternalSystem.[ActionAssembly]
							FROM SCH.ScheduleStepParameter
								LEFT JOIN tblDeliveryTypeDetail ON ScheduleStepParameter.ParameterValue = tblDeliveryTypeDetail.[Name]
								LEFT JOIN tblExternalSystem ON tblDeliveryTypeDetail.ExternalSystemID = tblExternalSystem.ExternalSystemID
							WHERE ScheduleStepID = SCH.ScheduleStep.ScheduleStepID
						) AS '@domainInfo',
						[XmlParameters] AS xmlParameters, 
						(
							SELECT --'true' AS '@json:Array',
								[ParameterKey] AS '@key',
								ParameterValue AS '@value'
							FROM SCH.ScheduleStepParameter
							WHERE ScheduleStepID = SCH.ScheduleStep.ScheduleStepID
							FOR XML PATH ('parameter'), TYPE
						)
					FROM SCH.ScheduleStep
					WHERE ScheduleStep.ScheduleID = SCh.Schedule.ScheduleID
					ORDER BY RunOrder
					FOR XML PATH ('scheduleStep'), TYPE
			)
		FROM SCH.Schedule 
			INNER JOIN SCH.Frequency ON SCH.Schedule.FrequencyID = SCH.Frequency.FrequencyID
			INNER JOIN SCH.Instance ON SCH.Schedule.InstanceID = SCH.Instance.InstanceID
		WHERE Active = 1 AND
			SCH.Instance.Server = @InstanceName AND
			(@ScheduleID IS NULL OR SCH.Schedule.ScheduleID = @ScheduleID) AND
			SCH.Schedule.[Status] = 'Active' AND
			(SCH.Schedule.EndDate IS NULL OR (SCH.Schedule.EndDate IS NOT NULL AND SCH.Schedule.EndDate > GETDATE()))
		FOR XML PATH ('schedule'), TYPE
	)  FOR XML PATH ('schedules'), TYPE
	)
END
GO

/****** Object:  StoredProcedure [SCH].[ScheduleStatus]    Script Date: 18/02/2023 12:12:20 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ScheduleStatus] -- [SCH].[ScheduleStatus] 'WIN-17RIAAGM8IK'
	@InstanceName nvarchar(50)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @Now datetime
SET @Now = GETDATE()
SELECT 
	SCH.Schedule.Name,
	SCH.Schedule.ScheduleID,
	ISNULL(SCH.Schedule.RestartOnThreshHold, 0) AS RestartOnThreshHold,
	CASE
		WHEN DATEDIFF(second, ISNULL(SCH.Schedule.LastRunCompleted, '1990-01-01'), GETDATE()) > (SCH.Frequency.DelayBeforeRedIndicator + CASE SCH.Frequency.Name 
																														WHEN 'Secondly' THEN SCH.Schedule.ScheduleFrequency  
																														WHEN 'Minutely' THEN SCH.Schedule.ScheduleFrequency * 60
																														ELSE 0 END) THEN 'Red'
		WHEN DATEDIFF(second, ISNULL(SCH.Schedule.LastRunCompleted, '1990-01-01'), GETDATE()) > (SCH.Frequency.DelayBeforeOrangeIndicator + CASE SCH.Frequency.Name 
																														WHEN 'Secondly' THEN SCH.Schedule.ScheduleFrequency  
																														WHEN 'Minutely' THEN SCH.Schedule.ScheduleFrequency * 60
																														ELSE 0 END) THEN 'Orange'
	END AS ScheduleStatus,
	ISNULL(SCH.Schedule.LastRunCompleted, DATEADD(YEAR, -1, GETDATE())) AS LastRun,
	CASE
		WHEN SCH.Frequency.Name = 'Secondly' THEN 'Every ' + CONVERT(nvarchar(50), SCH.Schedule.ScheduleFrequency) + ' second(s)'
		WHEN SCH.Frequency.Name = 'Minutely' THEN 'Every ' + CONVERT(nvarchar(50), SCH.Schedule.ScheduleFrequency) + ' minute(s)'
		WHEN SCH.Frequency.Name = 'Hourly' THEN 'Every ' + CONVERT(nvarchar(50), SCH.Schedule.ScheduleFrequency) + ' hour(s)'
		WHEN SCH.Frequency.Name = 'Daily' THEN 'Every ' + CONVERT(nvarchar(50), SCH.Schedule.ScheduleFrequency) + ' day(s)' 
		WHEN SCH.Frequency.Name = 'Weekly' THEN 'Every ' + CONVERT(nvarchar(50), SCH.Schedule.ScheduleFrequency) + ' week(s)'
		WHEN SCH.Frequency.Name = 'Monthly' THEN 'Every ' + CONVERT(nvarchar(50), SCH.Schedule.ScheduleFrequency) + ' month(s)'
		WHEN SCH.Frequency.Name = 'Specific' THEN 'Every ' + CONVERT(nvarchar(50), SCH.Schedule.ScheduleFrequency) + ' week(s), on ' +
			CASE 
				WHEN Monday = 1 THEN 'Monday,'
				WHEN Tuesday = 1 THEN 'Tuesday,'
				WHEN Wednesday = 1 THEN 'Wednesday,'
				WHEN Thursday = 1 THEN 'Thursday,'
				WHEN Friday = 1 THEN 'Friday,'
				WHEN Saturday = 1 THEN 'Saturday,'
				WHEN Sunday = 1 THEN 'Sunday'
			END
	END AS ScheduleFrequency
FROM SCH.Schedule
	INNER JOIN SCH.Frequency ON SCH.Schedule.FrequencyID = SCH.Frequency.FrequencyID
	INNER JOIN SCH.Instance ON SCH.Schedule.InstanceID = SCH.Instance.InstanceID
WHERE 
	SCH.Instance.[Server] = @InstanceName AND
	SCH.Schedule.Active = 1 AND
	SCH.Schedule.StartDate < GETDATE() AND 
	(SCH.Schedule.EndDate > GETDATE() OR SCH.Schedule.EndDate IS NULL) AND
	(
		DATEDIFF(second, ISNULL(SCH.Schedule.LastRunCompleted, '1990-01-01'), GETDATE()) > (SCH.Frequency.DelayBeforeOrangeIndicator + CASE SCH.Frequency.Name 
																														WHEN 'Secondly' THEN SCH.Schedule.ScheduleFrequency  
																														WHEN 'Minutely' THEN SCH.Schedule.ScheduleFrequency * 60
																														ELSE 0 END)
		OR
		DATEDIFF(second, ISNULL(SCH.Schedule.LastRunCompleted, '1990-01-01'), GETDATE()) > (SCH.Frequency.DelayBeforeRedIndicator + CASE SCH.Frequency.Name 
																														WHEN 'Secondly' THEN SCH.Schedule.ScheduleFrequency  
																														WHEN 'Minutely' THEN SCH.Schedule.ScheduleFrequency * 60
																														ELSE 0 END)
	) AND
	(
		SELECT COUNT(ScheduleStepID)
		FROM SCH.ScheduleStep
		WHERE SCH.ScheduleStep.ScheduleID = SCH.Schedule.ScheduleID
	) > 0 AND
	(
		(CONVERT(time, SCH.Schedule.RunBetweenStartTime) < CONVERT(time, GETDATE()) AND CONVERT(time, SCH.Schedule.RunBetweenEndTime) > CONVERT(time, GETDATE()))
		OR 
		(SCH.Schedule.RunBetweenStartTime IS NULL AND SCH.Schedule.RunBetweenEndTime IS NULL)
	)

	
	
GO

/****** Object:  StoredProcedure [SCH].[ScheduleStep_Delete]    Script Date: 18/02/2023 12:12:20 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ScheduleStep_Delete]
	@ScheduleStepID int,
	@Message nvarchar(4000) OUTPUT
AS

SET NOCOUNT ON

SET XACT_ABORT ON
BEGIN TRANSACTION

	DELETE FROM ScheduleStepParameter
	WHERE ScheduleStepID = @ScheduleStepID

	DELETE FROM ScheduleStep
	WHERE ScheduleStepID = @ScheduleStepID

COMMIT TRANSACTION
SET XACT_ABORT OFF
RETURN

GO

/****** Object:  StoredProcedure [SCH].[ScheduleStep_Insert]    Script Date: 18/02/2023 12:12:20 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ScheduleStep_Insert]
	@CreatedBy nvarchar(50),	
	@ScheduleID int,
	@Name nvarchar(150),
	@Library nvarchar(250),	
	@ClassName nvarchar(250),
	@RunOrder int,
	@ParamKey nvarchar(50),
	@ParamValue nvarchar(max),
	@ConfigFile nvarchar(250),
	@XMLParamValue nvarchar(max),
	@RunInPrivateDomain bit,
	@Message nvarchar(4000) OUTPUT
AS
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRANSACTION
SET @Message = N''
IF LEN(@Message) = 0 BEGIN
	DECLARE @ScheduleStepID int
	INSERT INTO [SCH].[ScheduleStep] (
		ScheduleID,
		Created,
		CreatedBy,
		LastModified,
		LastModifiedBy,
		Name,
		Library,
		ConfigFile,
		ClassName,
		RunOrder,
		XmlParameters,
		RunInPrivateDomain
	)
	VALUES (
		@ScheduleID,
		GETDATE(),
		@CreatedBy,
		GETDATE(),
		@CreatedBy,
		@Name,
		@Library,
		@ConfigFile,
		@ClassName,
		@RunOrder,
		@XMLParamValue,
		@RunInPrivateDomain
	)
	SET @ScheduleStepID = SCOPE_IDENTITY()
	INSERT INTO [SCH].[ScheduleStepParameter]
		   ([Created]
		   ,[CreatedBy]
		   ,[LastModified]
		   ,[LastModifiedBy]
		   ,[ScheduleStepID]
		   ,[ParameterKey]
		   ,[ParameterValue])
	 VALUES(
			GETDATE()
			,@CreatedBy
			,GETDATE()
			,@CreatedBy
			,@ScheduleStepID
			,@ParamKey
			,@ParamValue)	
	
END
COMMIT TRANSACTION
SET XACT_ABORT OFF
RETURN
GO

/****** Object:  StoredProcedure [SCH].[ScheduleStep_Update]    Script Date: 18/02/2023 12:12:20 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ScheduleStep_Update]
	@LastModifiedBy nvarchar(50),	
	@ScheduleStepID int,
	@Name nvarchar(150),
	@Library nvarchar(250),	
	@ClassName nvarchar(250),
	@RunOrder int,
	@ScheduleStepParamID int,
	@ParamKey nvarchar(50),
	@ParamValue nvarchar(max),
	@ConfigFile nvarchar(250),
	@XMLParamValue nvarchar(max),
	@RunInPrivateDomain bit,
	@Message nvarchar(4000) OUTPUT
AS
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRANSACTION
SET @Message = N''
IF LEN(@Message) = 0 BEGIN
	UPDATE SCH.ScheduleStep
		SET LastModified = GETDATE(),
			LastModifiedBy = @LastModifiedBy,
			Name = @Name,
			Library = @Library,
			ClassName = @ClassName,
			RunOrder = @RunOrder,
			ConfigFile = @ConfigFile,
			XmlParameters = @XMLParamValue,
			RunInPrivateDomain = @RunInPrivateDomain
	WHERE SCH.ScheduleStep.ScheduleStepID = @ScheduleStepID
	
	UPDATE SCH.ScheduleStepParameter
		SET LastModified = GETDATE(),
			LastModifiedBy = @LastModifiedBy,
			ParameterKey = @ParamKey,
			ParameterValue = @ParamValue
	WHERE SCH.ScheduleStepParameter.ScheduleStepParameterID = @ScheduleStepParamID
	
END
COMMIT TRANSACTION
SET XACT_ABORT OFF
RETURN
GO

/****** Object:  StoredProcedure [SCH].[ScheduleStepParameters]    Script Date: 18/02/2023 12:12:20 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ScheduleStepParameters]
	@ScheduleStepID int
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT SCH.ScheduleStepParameter.*
	FROM SCH.ScheduleStepParameter
	WHERE SCH.ScheduleStepParameter.SchedulestepID = @ScheduleStepID

END

GO

/****** Object:  StoredProcedure [SCH].[ScheduleSteps]    Script Date: 18/02/2023 12:12:20 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[ScheduleSteps]
	@ScheduleID int
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT SCH.ScheduleStep.*
	FROM SCH.ScheduleStep
	WHERE SCH.ScheduleStep.ScheduleID = @ScheduleID
	ORDER BY SCH.ScheduleStep.RunOrder

END
GO

/****** Object:  StoredProcedure [SCH].[StatusLog_Delete]    Script Date: 18/02/2023 12:12:20 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [SCH].[StatusLog_Delete]
	@ScheduleID int
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DELETE FROM 
		SCH.StatusLog
	WHERE 
		ScheduleID = @ScheduleID AND
		Created < dateadd(day,-30,getdate())

	RETURN

GO

/****** Object:  StoredProcedure [SCH].[StatusLog_Insert]    Script Date: 18/02/2023 12:12:21 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[StatusLog_Insert]
	@InstanceName nvarchar(50),
	@ScheduleName nvarchar(500),
	@Severity int,
	@LogType int,
	@Message nvarchar(max)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	DECLARE @InstanceID int
	DECLARE @ScheduleID int
	
	SET @InstanceID = (SELECT InstanceID FROM SCH.Instance WHERE Server = @InstanceName)
	
	IF @ScheduleName = 'General' BEGIN
		SET @ScheduleID = NULL
	END ELSE BEGIN
		SET @ScheduleID = (SELECT ScheduleID FROM SCH.Schedule WHERE Name = @ScheduleName) 
	END
	INSERT INTO SCH.StatusLog(CreatedBy, LastModifiedBy, Severity, InstanceID, ScheduleID, [Message], LogType)
	VALUES('Sandfield.Scheduling', 'Sandfield.Scheduling', @Severity, @InstanceID, @ScheduleID, @Message, @LogType)
	RETURN
	

GO

/****** Object:  StoredProcedure [SCH].[WorkWithList_ScheduleGroups]    Script Date: 18/02/2023 12:12:21 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[WorkWithList_ScheduleGroups]
	@Entity nvarchar(MAX),
	@Name nvarchar(50) = NULL,
	@UserID int,
	@OrderByColumn int,
	@Order nvarchar(50)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT TableValue AS EntityCode
INTO #EntityCodes
FROM [WKW].CSV_ToTable(@Entity)
SELECT 
	ScheduleGroup.ScheduleGroupID,
	ScheduleGroup.[Name]
FROM SCH.ScheduleGroup
WHERE 
	(@Entity IS NULL OR SCH.ScheduleGroup.EntityID IN (SELECT EntityID FROM SALCOR.Entity WHERE EntityCode IN (SELECT EntityCode FROM #EntityCodes))) AND
	(@Name IS NULL OR SCH.ScheduleGroup.[Name] LIKE N'%' + @Name +'%') AND
	SCH.ScheduleGroup.[Status] != 'Deleted'
ORDER BY 
	CASE 
		WHEN @OrderByColumn = 0 AND @Order LIKE '%DESC%' THEN Name
		ELSE NULL
	END DESC,
	CASE 
		WHEN @OrderByColumn = 0 AND @Order LIKE '%ASC%' THEN Name
		ELSE NULL
	END ASC,
	Name
RETURN

GO

/****** Object:  StoredProcedure [SCH].[WorkWithList_Schedules]    Script Date: 18/02/2023 12:12:21 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[WorkWithList_Schedules] 
	--@EntityID int,
	@Entity nvarchar(MAX),
	@Name nvarchar(50),
	@UserID int,
	@OrderByColumn int,
    @Order nvarchar(50),
	@ParentID int = NULL,
	@Partner nvarchar(50) = NULL,
	@ShowInactive bit = NULL
WITH RECOMPILE AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT TableValue AS EntityCode
INTO #EntityCodes
FROM [WKW].CSV_ToTable(@Entity)
SELECT ParentID, Name, TotalSchedules , WorkWithObject FROM (
SELECT 
	SCH.ScheduleGroup.ScheduleGroupID AS ParentID,
	SCH.ScheduleGroup.Name AS Name,
	COUNT(SCH.Schedule.ScheduleID) AS TotalSchedules, 
	'MSG.ScheduleGroup' AS WorkWithObject 
FROM SCH.ScheduleGroup
	LEFT OUTER JOIN SCH.Schedule ON SCH.ScheduleGroup.ScheduleGroupID = SCH.Schedule.ScheduleGroupID
	LEFT OUTER JOIN (
		SELECT DISTINCT ScheduleStep.ScheduleID
		FROM SCH.ScheduleStep
		JOIN SCH.ScheduleStepParameter ON ScheduleStep.ScheduleStepID = ScheduleStepParameter.ScheduleStepID
		JOIN MSG.DeliveryTypeDetail ON ScheduleStepParameter.ParameterValue = DeliveryTypeDetail.Name
		JOIN EDI.[Partner] ON DeliveryTypeDetail.PartnerID = [Partner].PartnerID
		WHERE [Partner].[Name] = @Partner
	) PartnerMatch ON Schedule.ScheduleID = PartnerMatch.ScheduleID
WHERE  
	--(@EntityID IS NULL OR SCH.ScheduleGroup.EntityID = @EntityID) AND
	(@Entity IS NULL OR SCH.ScheduleGroup.EntityID IN (SELECT EntityID FROM SALCOR.Entity WHERE EntityCode IN (SELECT EntityCode FROM #EntityCodes))) AND
	(@Name IS NULL OR Schedule.Name LIKE N'%' + @Name +'%') AND
	(Schedule.[Status] = 'Active') AND
	(@Partner IS NULL OR PartnerMatch.ScheduleID IS NOT NULL) AND
	(@ShowInactive IS NULL OR @ShowInactive = 1 OR (@ShowInactive = 0 AND Schedule.Active = 1))
GROUP BY 
	SCH.ScheduleGroup.ScheduleGroupID, 
	SCH.ScheduleGroup.Name
	) results
ORDER BY 
	CASE 
		WHEN @OrderByColumn = 0 AND @Order LIKE '%DESC%' THEN Name
		WHEN @OrderByColumn = 1 AND @Order LIKE '%DESC%' THEN CAST(TotalSchedules AS sql_variant)
		ELSE NULL
	END DESC,
	CASE 
		WHEN @OrderByColumn = 0 AND @Order LIKE '%ASC%' THEN Name
		WHEN @OrderByColumn = 1 AND @Order LIKE '%ASC%' THEN CAST(TotalSchedules AS sql_variant)
		ELSE NULL
	END ASC,
	Name
RETURN
;

GO

/****** Object:  StoredProcedure [SCH].[WorkWithList_Schedules_Detail]    Script Date: 18/02/2023 12:12:21 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[WorkWithList_Schedules_Detail]
	@ParentID nvarchar(50),
	--@EntityID int,
	@Entity nvarchar(MAX),
	@Name nvarchar(50) = NULL,
	@UserID int = NULL,
	@OrderByColumn int = 0,
    @Order nvarchar(50),
	@Partner nvarchar(50) = NULL,
	@ShowInactive bit = NULL
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT ScheduleID, Name, Frequency,
Active, StartDate, EndDate, LastRunCompleted, NextRunTime, [Server], NumberOfScheduleSteps
 FROM (
SELECT 
	Schedule.ScheduleID,
	Schedule.Name,
	--Schedule.Name + ' - ' + CONVERT(nvarchar(50),Schedule.ScheduleFrequency) + ' ' + Frequency.Name [Name], 
	--Schedule.ScheduleFrequency,
	Frequency.Name + ' (' + CAST(Schedule.ScheduleFrequency as nvarchar) + ')' AS Frequency,
	Schedule.Active,
	CONVERT(VARCHAR(10), Schedule.StartDate, 103) + ' ' + CONVERT(VARCHAR(5), Schedule.StartDate, 108) AS StartDate,
	CONVERT(VARCHAR(10), Schedule.EndDate, 103) + ' ' + CONVERT(VARCHAR(5), Schedule.EndDate, 108) AS EndDate,
	CONVERT(VARCHAR(10), Schedule.LastRunCompleted, 103) + ' ' + CONVERT(VARCHAR(5), Schedule.LastRunCompleted, 108) AS LastRunCompleted,
	CONVERT(VARCHAR(10), Schedule.NextRunTime, 103) + ' ' + CONVERT(VARCHAR(5), Schedule.NextRunTime, 108) AS NextRunTime,
	Instance.[Server],
	(SELECT COUNT(*) FROM SCH.ScheduleStep ss WHERE ss.ScheduleID = SCH.Schedule.ScheduleID) AS NumberOfScheduleSteps,
	Frequency.Name AS FreqName
FROM SCH.Schedule
	LEFT JOIN SCH.ScheduleGroup ON SCH.Schedule.ScheduleGroupID = SCH.ScheduleGroup.ScheduleGroupID
	LEFT JOIN SCH.Frequency ON Schedule.FrequencyID = Frequency.FrequencyID
	LEFT JOIN SCH.Instance ON SCH.Schedule.InstanceID = SCH.Instance.InstanceID
	LEFT OUTER JOIN (
		SELECT DISTINCT ScheduleStep.ScheduleID
		FROM SCH.ScheduleStep
		JOIN SCH.ScheduleStepParameter ON ScheduleStep.ScheduleStepID = ScheduleStepParameter.ScheduleStepID
		JOIN MSG.DeliveryTypeDetail ON ScheduleStepParameter.ParameterValue = DeliveryTypeDetail.Name
		JOIN EDI.[Partner] ON DeliveryTypeDetail.PartnerID = [Partner].PartnerID
		WHERE [Partner].[Name] = @Partner
	) PartnerMatch ON Schedule.ScheduleID = PartnerMatch.ScheduleID
WHERE 
	COALESCE(SCH.ScheduleGroup.ScheduleGroupID, 0) = COALESCE(@ParentID, 0)	AND 
	(
		@Name IS NULL
		OR 
		Schedule.Name LIKE N'%' + @Name +'%'
	) AND
	Schedule.[Status] = 'Active' AND
	(@Partner IS NULL OR PartnerMatch.ScheduleID IS NOT NULL) AND
	(@ShowInactive IS NULL OR @ShowInactive = 1 OR (@ShowInactive = 0 AND Schedule.Active = 1))
) results
ORDER BY 
	CASE 
		WHEN @OrderByColumn = 0 AND @Order LIKE '%DESC%' THEN Name
		WHEN @OrderByColumn = 1 AND @Order LIKE '%DESC%' THEN FreqName
		WHEN @OrderByColumn = 2 AND @Order LIKE '%DESC%' THEN Active
		WHEN @OrderByColumn = 3 AND @Order LIKE '%DESC%' THEN CONVERT(DATETIME,StartDate,103)
		WHEN @OrderByColumn = 4 AND @Order LIKE '%DESC%' THEN CONVERT(DATETIME,EndDate,103)
		WHEN @OrderByColumn = 5 AND @Order LIKE '%DESC%' THEN CONVERT(DATETIME,LastRunCompleted,103)
		WHEN @OrderByColumn = 6 AND @Order LIKE '%DESC%' THEN CONVERT(DATETIME,NextRunTime,103)
		WHEN @OrderByColumn = 7 AND @Order LIKE '%DESC%' THEN Server
		WHEN @OrderByColumn = 8 AND @Order LIKE '%DESC%' THEN CAST(NumberOfScheduleSteps AS sql_variant)
		ELSE NULL
	END DESC,
	CASE 
		WHEN @OrderByColumn = 0 AND @Order LIKE '%ASC%' THEN Name
		WHEN @OrderByColumn = 1 AND @Order LIKE '%ASC%' THEN FreqName
		WHEN @OrderByColumn = 2 AND @Order LIKE '%ASC%' THEN Active
		WHEN @OrderByColumn = 3 AND @Order LIKE '%ASC%' THEN CONVERT(DATETIME,StartDate,103)
		WHEN @OrderByColumn = 4 AND @Order LIKE '%ASC%' THEN CONVERT(DATETIME,EndDate,103)
		WHEN @OrderByColumn = 5 AND @Order LIKE '%ASC%' THEN CONVERT(DATETIME,LastRunCompleted,103)
		WHEN @OrderByColumn = 6 AND @Order LIKE '%ASC%' THEN CONVERT(DATETIME,NextRunTime,103)
		WHEN @OrderByColumn = 7 AND @Order LIKE '%ASC%' THEN Server
		WHEN @OrderByColumn = 8 AND @Order LIKE '%ASC%' THEN CAST(NumberOfScheduleSteps AS sql_variant)
		ELSE NULL
	END ASC,
	Name, FreqName
RETURN

GO

/****** Object:  StoredProcedure [SCH].[WorkWithList_ScheduleStatusLog]    Script Date: 18/02/2023 12:12:21 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[WorkWithList_ScheduleStatusLog]
	--@EntityID int,
	@Entity nvarchar(MAX),
	@UserID int,
	@OrderByColumn int,
	@Order nvarchar(50),
	@DateFilter nvarchar(50),
	@Start datetime,
	@End datetime,
	@Severity nvarchar(50) = NULL,
	@ScheduleID int = NULL
WITH RECOMPILE AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
BEGIN
	SELECT TOP(10000) *
	INTO #TempDB
	FROM [SCH].[StatusLog]
	ORDER BY Created DESC
END
BEGIN
	SELECT DISTINCT --TOP(1000) 
		#TempDB.StatusLogID,
		SUBSTRING([SCH].Schedule.[Name], 0, 100),
		#TempDB.Created, 
		CASE 
			WHEN Severity = 0 THEN NULL
			WHEN Severity = 1 THEN 'Error'
			WHEN Severity = 2 THEN 'Chatter' 
		END AS Severity,
		#TempDB.[Message],
		#TempDB.ScheduleID
	FROM
		#TempDB
		--[SCH].StatusLog
	LEFT JOIN [SCH].Schedule ON [SCH].Schedule.ScheduleID = #TempDB.ScheduleID
	WHERE
		(#TempDB.ScheduleID = @ScheduleID OR @ScheduleID IS NULL) AND
		(@DateFilter IS NULL OR 
			(
				@DateFilter = N'Created' AND
				(@Start IS NULL OR #TempDB.Created >= @Start) AND
				(@End IS NULL OR #TempDB.Created <= @End)
			)
		) AND
		(@Severity IS NULL OR @Severity = Severity)
	ORDER BY #TempDB.Created DESC
	DROP TABLE #TempDB
END
GO



/****** Object:  StoredProcedure [SCH].[Schedule_Import_Internal]    Script Date: 18/02/2023 12:12:16 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Import_Internal] 
	@ScheduleDocHandle	int,
	@Doc				xml,
	@XPathQuery			nvarchar(max),
	@CreatedBy			nvarchar(255),
	@InstanceID			int = null,
	@ScheduleGroupID	int,
	@ScheduleID			int OUTPUT,
	@Message			nvarchar(max) OUTPUT
AS
SET NOCOUNT ON

IF @InstanceID IS NULL BEGIN
	SELECT TOP 1 @InstanceID = InstanceID FROM SCH.Instance
END
DECLARE @EngineLocation nvarchar(MAX)
SELECT @EngineLocation = [Value] FROM tblSetting WHERE KeyName = 'CrossfireEngineLocation'
SET @Message = ''
-------------------------------------------------------------------------------
--	INSERT SCHEDULES
-------------------------------------------------------------------------------
DECLARE	@ScheduleName nvarchar(500),
		@Description nvarchar(2000),
		@FrequencyID int,
		@StartDate datetime,
		@EndDate datetime,
		@ScheduleFrequency int,
		@EveryNumberOfWeeks int,
		@Monday bit,
		@Tuesday bit,
		@Wednesday bit,
		@Thursday bit,
		@Friday bit,
		@Saturday bit,
		@Sunday bit,
		@Active bit,
		@XmlScheduleGroupID int,
		@Frequency nvarchar(50),
		@XmlFrequencyID int,
		@EntityID int,
		@ScheduleGroupName nvarchar(max)
PRINT 'Locating Schedule'
DECLARE csr CURSOR FOR 
	SELECT		
		Name,
		Description,
		FrequencyID,
	    Frequency,
		StartDate,
		EndDate,
		ScheduleFrequency,
		EveryNumberOfWeeks,
		Monday,
		Tuesday,
		Wednesday,
		Thursday,
		Friday,
		Saturday,
		Sunday,
		Active,
		ScheduleGroupID,
		EntityID,
		ScheduleGroupName
	FROM OPENXML (@ScheduleDocHandle, '/Schedule',1) WITH  
	(	Name nvarchar(500)				   '@Name',
		Description nvarchar(2000)		   '@Description',
		FrequencyID int					   '@FrequencyID',
		Frequency nvarchar(50)			   '@Frequency',
		StartDate datetime				   '@StartDate',
		EndDate datetime				   '@EndDate',
		ScheduleFrequency int			   '@ScheduleFrequency',
		EveryNumberOfWeeks int			   '@EveryNumberOfWeeks',
		Monday bit						   '@Monday',
		Tuesday bit						   '@Tuesday',
		Wednesday bit					   '@Wednesday',
		Thursday bit					   '@Thursday',
		Friday bit						   '@Friday',
		Saturday bit					   '@Saturday',
		Sunday bit						   '@Sunday',
		Active bit						   '@Active',
		ScheduleGroupID int				   '@ScheduleGroupID',
		EntityID int					   '@EntityID',
		ScheduleGroupName nvarchar(500)    '@ScheduleGroupName'
	) Schedule
OPEN csr
FETCH NEXT FROM csr INTO @ScheduleName, @Description, @XmlFrequencyID, @Frequency, @StartDate, @EndDate, @ScheduleFrequency, @EveryNumberOfWeeks, 
		@Monday, @Tuesday, @Wednesday, @Thursday, @Friday, @Saturday,@Sunday, @Active, @XmlScheduleGroupID, @EntityID, @ScheduleGroupName
WHILE @@FETCH_STATUS = 0 BEGIN
	PRINT 'Importing Schedule'
	DECLARE @ScheduleStepID int
	SET @ScheduleID = NULL
	SET @ScheduleStepID = NULL
	EXECUTE [SCH].[Schedule_CheckUniqueness] NULL, @ScheduleName, @Message OUTPUT
	IF LEN(@Message) <> 0 BEGIN
	    GOTO ErrHandler
	END
	IF COALESCE(@XmlFrequencyID, 0) = 0 BEGIN
		SELECT @XmlFrequencyID = FrequencyID FROM sch.frequency WHERE NAME = @Frequency
	END
	SET @FrequencyID = @XmlFrequencyID
	
	IF COALESCE(@FrequencyID, 0) = 0 BEGIN
		SET @Message = @Message + ' Missing frequency on schedule.'
	    GOTO ErrHandler
	END

	PRINT 'Checking ScheduleGroup'
	IF @ScheduleGroupID IS NULL BEGIN 
		SELECT @ScheduleGroupID = ScheduleGroupID FROM SCH.ScheduleGroup WHERE Name = @ScheduleGroupName
	END 
	IF @ScheduleGroupID IS NULL BEGIN 
		SELECT @ScheduleGroupID = ScheduleGroupID FROM SCH.ScheduleGroup WHERE ScheduleGroupID = @XmlScheduleGroupID
	END 
	IF @ScheduleGroupID IS NULL BEGIN
		EXEC [SCH].[ScheduleGroup_Insert] @ScheduleName, @EntityID, @CreatedBy, @ScheduleGroupID OUT, @Message OUT
	END
	 
	PRINT '@ScheduleGroupID' + CAST(@ScheduleGroupID as nvarchar)
	INSERT sch.Schedule (	
		CreatedBy,
		LastModifiedBy,
		Name,
		Description,
		FrequencyID,
		Active,
		StartDate,
		EndDate,
		ScheduleFrequency,
		EveryNumberOfWeeks,
		Monday,
		Tuesday,
		Wednesday,
		Thursday,
		Friday,
		Saturday,
		Sunday,
		ScheduleGroupID,
		InstanceID,
		[Status]
	)	
	SELECT		
		@CreatedBy,
		@CreatedBy,
		@ScheduleName,
		@Description,
		@FrequencyID,	
		@Active,
		@StartDate,
		@EndDate,
		@ScheduleFrequency,
		@EveryNumberOfWeeks,
		@Monday,
		@Tuesday,
		@Wednesday,
		@Thursday,
		@Friday,
		@Saturday,
		@Sunday,
		@ScheduleGroupID,
		@InstanceID,
		'Active'
	IF @@ROWCOUNT = 1 BEGIN
	 SET @ScheduleID = SCOPE_IDENTITY()
	END
	ELSE BEGIN
	   SET @Message = @Message + ' Failed to add schedule record'
	   GOTO ErrHandler
	END
-------------------------------------------------------------------------------
--	INSERT SCHEDULES STEPS
-------------------------------------------------------------------------------
	DECLARE	@StepName nvarchar(50),
			@ConfigFile nvarchar(250),
			@Library nvarchar(250),
			@ClassName nvarchar(250),
			@RunOrder int,
			@XmlParameters nvarchar(max)
	
	DECLARE csrStep CURSOR FOR 
		SELECT
			[Name],
			ConfigFile,
			Library,
			ClassName,
			RunOrder,
			XmlParameters
		FROM OPENXML (@ScheduleDocHandle, '/Schedule/Steps/Step', 1)
		WITH (
			[Name] nvarchar(50),
			[ConfigFile] nvarchar(250),
			[Library] nvarchar(250),
			[ClassName] nvarchar(250),
			[RunOrder] int,
			[XmlParameters] nvarchar(max)
		) AS NewFunction
	OPEN csrStep
	FETCH NEXT FROM csrStep INTO @StepName, @ConfigFile, @Library, @ClassName, @RunOrder, @XmlParameters
	WHILE @@FETCH_STATUS = 0 BEGIN
		SET @ScheduleStepID = NULL
		INSERT sch.ScheduleStep (	
			Created,
			CreatedBy,
			LastModified,
			LastModifiedBy,
			Name,
			ScheduleID,
			ConfigFile,
			Library,
			ClassName,
			RunOrder,
			XmlParameters
		)
		SELECT
			GETDATE(),
			@CreatedBy,
			GETDATE(),
			@CreatedBy,
			@StepName,
			@ScheduleID,
			REPLACE(@ConfigFile, 'D:\Engine', @EngineLocation),
			REPLACE(@Library, 'D:\Engine', @EngineLocation),
			@ClassName,
			@RunOrder,
			@XmlParameters
		
		IF @@ROWCOUNT = 1 BEGIN
			SET @ScheduleStepID = SCOPE_IDENTITY()
		END 
		ELSE BEGIN
		   SET @Message = @Message + ' Failed to add schedule step record'
		END
			-------------------------------------------------------------------------------
			--	INSERT SCHEDULES STEP PARAMETERS
			-------------------------------------------------------------------------------
				DECLARE	@ParameterKey nvarchar(255),
						@ParameterValue nvarchar(max),
						@ScheduleStepParameterID int
				DECLARE csrStepParameter CURSOR FOR 
					SELECT P.Params.query('ParamKey').value('.','nvarchar(255)'), P.Params.query('ParamValue').value('.','nvarchar(max)')
					FROM @Doc.nodes('/Schedule/Steps/Step/Parameters/Parameter') P(Params)
				OPEN csrStepParameter
				FETCH NEXT FROM csrStepParameter INTO @ParameterKey, @ParameterValue
				WHILE @@FETCH_STATUS = 0 BEGIN
					Print @ParameterKey + ' : ' + @ParameterValue
					INSERT sch.ScheduleStepParameter (	
						CreatedBy,
						LastModifiedBy,
						ScheduleStepID,
						ParameterKey,
						ParameterValue
					)
					VALUES (
						@CreatedBy,
						@CreatedBy,
						@ScheduleStepID,
						@ParameterKey,
						LTRIM(RTRIM(@ParameterValue))
					)
					IF @@ROWCOUNT = 1 BEGIN
						SET @ScheduleStepParameterID = SCOPE_IDENTITY()
						PRINT '@ScheduleStepParameterID'
						PRINT @ScheduleStepParameterID
					END 
					ELSE BEGIN
					   SET @Message = @Message + ' Failed to add schedule step parameter record'
					END
					
					FETCH NEXT FROM csrStepParameter INTO @ParameterKey, @ParameterValue
				END
				CLOSE csrStepParameter
				DEALLOCATE csrStepParameter
			FETCH NEXT FROM csrStep INTO @StepName, @ConfigFile, @Library, @ClassName, @RunOrder, @XmlParameters
		END
		CLOSE csrStep
		DEALLOCATE csrStep
	FETCH NEXT FROM csr INTO @ScheduleName, @Description, @XmlFrequencyID, @Frequency, @StartDate, @EndDate, @ScheduleFrequency, @EveryNumberOfWeeks, 
		@Monday, @Tuesday, @Wednesday, @Thursday, @Friday, @Saturday,@Sunday, @Active, @XmlScheduleGroupID, @EntityID
END
CLOSE csr
DEALLOCATE csr
RETURN
ErrHandler:
	CLOSE csr
	DEALLOCATE csr
	RETURN
GO

/****** Object:  StoredProcedure [SCH].[Schedule_Insert]    Script Date: 18/02/2023 12:12:16 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Insert]
	@Name nvarchar(500),
	@InstanceID int,
	@ScheduleGroupID int,
	@Description nvarchar(2000),
	@FrequencyID int,
	@Active bit,
	@RestartOnThreshHold bit,
	@StartDate datetime,
	@EndDate datetime,
	@RunBetweenStartTime datetime,
	@RunBetweenEndTime datetime,
	@ScheduleFrequency smallint,
	@DelayBetweenSteps smallint,
	@EveryNumberOfWeeks smallint,
	@Monday bit,
	@Tuesday bit,
	@Wednesday bit,
	@Thursday bit,
	@Friday bit,
	@Saturday bit,
	@Sunday bit,
	@CreatedBy nvarchar(50),
	@EnableDebug bit = 0,
	@ScheduleID int OUTPUT,
	@Message nvarchar(4000) OUTPUT
AS
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRANSACTION
SET @Message = N''
EXECUTE [SCH].[Schedule_CheckUniqueness] NULL, @Name, @Message OUTPUT
IF LEN(@Message) = 0 BEGIN
	INSERT INTO Schedule (
		Name,
		InstanceID,
		ScheduleGroupID,
		Description,
		FrequencyID,
		Active,
		RestartOnThreshHold,
		StartDate,
		EndDate,
		ScheduleFrequency,
		DelayBetweenSteps,
		EveryNumberOfWeeks,
		Monday,
		Tuesday,
		Wednesday,
		Thursday,
		Friday,
		Saturday,
		Sunday,
		CreatedBy,
		LastModifiedBy,
		[Status],
		LastRunCompleted,
		RunBetweenStartTime,
		RunBetweenEndTime,
		EnableDebug
	)
	VALUES (
		@Name,	
		@InstanceID,
		@ScheduleGroupID,
		@Description,
		@FrequencyID,
		@Active,
		@RestartOnThreshHold,
		@StartDate,
		@EndDate,
		@ScheduleFrequency,
		@DelayBetweenSteps,
		@EveryNumberOfWeeks,
		@Monday,
		@Tuesday,
		@Wednesday,
		@Thursday,
		@Friday,
		@Saturday,
		@Sunday,
		@CreatedBy,
		@CreatedBy,
		'Active',
		GETDATE(),
		@RunBetweenStartTime,
		@RunBetweenEndTime,
		@EnableDebug
	)
	SET @ScheduleID = SCOPE_IDENTITY()
End
COMMIT TRANSACTION
SET XACT_ABORT OFF
GO

/****** Object:  StoredProcedure [SCH].[Schedule_Import]    Script Date: 18/02/2023 12:12:16 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SCH].[Schedule_Import] 
	@ScheduleXML		nvarchar(max),
	@InstanceID			int,
	@ScheduleGroupID	int,
	@CreateBy			nvarchar(255),
	@ScheduleID			int OUTPUT,
	@Message			nvarchar(max) OUTPUT
AS
--Exists to store error messages as table variables aren't rolled back
DECLARE @Errors TABLE(ErrorMessage varchar(max))
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRANSACTION
DECLARE @Doc xml = TRY_CAST(@ScheduleXML AS xml)
DECLARE @Root nvarchar(max) = CAST(@Doc.query('/Schedule') AS nvarchar(max))
IF @Root = '' OR @Root IS NULL BEGIN
	SET @Message = 'The xml supplied was invalid or did not contain the correct root node.'
	ROLLBACK TRANSACTION
	RETURN
END
DECLARE @TempMessage nvarchar(max)
DECLARE @DocHandle int
EXEC sp_xml_preparedocument @DocHandle OUTPUT, @Doc
IF EXISTS(SELECT 1 FROM OPENXML (@DocHandle, N'/Schedule', 1)) BEGIN
	PRINT N'Importing scheudle'
	EXECUTE SCH.Schedule_Import_Internal @DocHandle, @Doc, N'/Schedule', @CreateBy, @InstanceID, @ScheduleGroupID, @ScheduleID OUTPUT, @TempMessage OUTPUT
	IF LEN(@TempMessage) > 0 BEGIN
		INSERT INTO @Errors(ErrorMessage) 
		VALUES(@TempMessage) --Table variable won't be rolled back
		SET @ScheduleID = NULL
		GOTO Error
	END
END
EXEC sp_xml_removedocument @DocHandle
COMMIT TRANSACTION
SET XACT_ABORT OFF
RETURN
Error:
EXEC sp_xml_removedocument @DocHandle
ROLLBACK TRANSACTION
SET XACT_ABORT OFF
SET @Message = NULL
SELECT @Message = COALESCE(@Message + ' ', '') + ErrorMessage FROM @Errors --Load Error Message from table variable
RETURN

GO
